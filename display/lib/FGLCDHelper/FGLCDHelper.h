#ifndef __FGLCDHELPER_H__
#define __FGLCDHELPER_H__

#include <LiquidCrystal.h>

class FGLCDHelper {
  private:
    int _pin_rs;
    int _pin_enable;
    int _pin_d4;
    int _pin_d5;
    int _pin_d6;
    int _pin_d7;

  public:
    LiquidCrystal lcd;
    FGLCDHelper(int pin_rs, int pin_enable, int pin_d4, int pin_d5, int pin_d6, int pin_d7);
    void writeData(byte *data);
    void writeTwoData(byte *data);
    void writeMenu(byte *data);
};

#endif // __FGLCDHELPER_H__