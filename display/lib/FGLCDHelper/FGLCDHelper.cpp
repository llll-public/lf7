#ifndef FGLCDHELPER_CPP_
#define FGLCDHELPER_CPP_

#include "FGLCDHelper.h"
#include <LiquidCrystal.h>
#include <Wire.h>

FGLCDHelper::FGLCDHelper(int pin_rs, int pin_enable, int pin_d4, int pin_d5, int pin_d6, int pin_d7)
    : _pin_rs(pin_rs), _pin_enable(pin_enable), _pin_d4(pin_d4), _pin_d5(pin_d5), _pin_d6(pin_d6), _pin_d7(pin_d7),
      lcd(LiquidCrystal(_pin_rs, _pin_enable, _pin_d4, _pin_d5, _pin_d6, _pin_d7)) {}

/// @brief Write text to the LCD screen. First two bytes are the cursor location col, row (each plus 1)
/// @param data 
void FGLCDHelper::writeData(byte *data) {
    lcd.clear();
    lcd.setCursor((data[0]) - 1, (data[1]) - 1);
    for (int i = 2; i < data[i] != 0; i++) {
        byte b = data[i];
        lcd.write(b);
    }
    lcd.flush();
}

/// @brief Write two texts to the lcd screen. Texts are separated by 0x1D and will be written in col 0 and 1
/// @param data 
void FGLCDHelper::writeTwoData(byte *data) {
    // Create buffers on heap
    char *one = new char[16]{0};
    char *two = new char[16]{0};

    int oneIndex = 0;
    int twoIndex = 0;

    bool isOne = true;
    // Process data byte by byte and fill each text buffer accordingly
    for (int i = 0; i < data[i] != 0; i++) {
        byte b = data[i];
        if (b == 0x1D) {
            isOne = false; // We processed the first text, now its time to flip
        } else {
            if (isOne) {
                one[oneIndex] = (char)b;
                oneIndex++;
                oneIndex = oneIndex % 16; // Prevent buffer overflow
            } else {
                two[twoIndex] = (char)b;
                twoIndex++;
                twoIndex = twoIndex % 16; // Prevent buffer overflow
            }
        }
    }

    lcd.clear();
    
    // Write first text to lcd
    lcd.setCursor(0, 0);
    for (int i = 0; one[i] != 0; i++) {
        lcd.write(one[i]);
    }

    // Write second text to lcd
    lcd.setCursor(0, 1);
    for (int i = 0; two[i] != 0; i++) {
        lcd.write(two[i]);
    }
    lcd.flush();

    // Free buffer
    free(one);
    free(two);
}

/// @brief Helper function to write a menu data stream to the lcd 
/// @param data 
void FGLCDHelper::writeMenu(byte *data) {
    // First four bytes determine specific availabilities of functions
    bool canGoUp = data[0] - 1;
    bool canGoDown = data[1] - 1;
    bool canForward = data[2] - 1;
    bool canReturn = data[3] - 1;

    lcd.clear();

    // Write text to lcd
    lcd.setCursor(0, 0);
    for (int i = 4; data[i] != 0 && i < 20; i++) {
        lcd.write(data[i]);
    }

    // Write specific availabilities to lcd
    lcd.setCursor(0, 1);
    canGoDown &&lcd.write('v');

    lcd.setCursor(6, 1);
    canReturn &&lcd.write('<');

    lcd.setCursor(9, 1);
    canForward &&lcd.write('>');

    lcd.setCursor(15, 1);
    canGoUp &&lcd.write('^');

    lcd.flush();
}

#endif // FGLCDHELPER_CPP_