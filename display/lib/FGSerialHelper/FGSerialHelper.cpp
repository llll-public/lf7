#ifndef FGSERIALHELPER_CPP_
#define FGSERIALHELPER_CPP_

#include "FGSerialHelper.h"
#include <Arduino.h>
#include <SoftwareSerial.h>

FGSerialHelper *FGSerialHelper::instance = nullptr; // Declaration and initialization of the instance pointer

FGSerialHelper &FGSerialHelper::getInstance() { return *instance; }
/// @brief Initializer function
void FGSerialHelper::init() {
    s.begin(38400);
    this->changeMode(this->readMode);

    // Initialize callbacks and buffer with zeroes
    callbacks = new Callback[8];
    memset(callbacks, 0, 8);

    readBuffer = new byte[32];
    memset(readBuffer, 0, 32);
}

/// @brief Set desired mode of the serial interface
/// @param readMode Whether or not this instance should listen for input or not
void FGSerialHelper::changeMode(bool readMode) {
    this->readMode = readMode;
    if (readMode == true) {
        pinMode(this->pinIN, INPUT);
        pinMode(this->pinOUT, OUTPUT);
        s.listen();
    } else {
        pinMode(this->pinIN, OUTPUT);
        pinMode(this->pinOUT, OUTPUT);
        if (s.isListening()) {
            s.stopListening();
        }
    }
}

/// @brief Handles input and triggers correct callbacks
void FGSerialHelper::handle() {
    bool isFirst = true;
    int code = 0;

    // Clear buffer
    memset(readBuffer, 0, 32);
    int i = 0;

    s.listen();
    while (true) {
        // Wait for data to arrive
        while (s.available() == 0) {
            delay(50);
        }
        int b = s.read();
        if (b < 0) {
            // Error code occured
            Serial.println("stopped");
            continue;
        }

        // First byte is the header
        if (isFirst) {
            if (b < 1 || b > 16) {
                break;
            }
            code = b;
            isFirst = false;
            continue;
        }

        // Terminate on 0 byte
        if (b == 0) {
            break;
        }

        // Add read byte to buffer, wrap after 32 bytes;
        readBuffer[i] = b;
        i++;
        i = i % 31;
    }
    s.stopListening();

    // Check if callback for that code is set
    if (code == 0 || this->callbacks[code - 1] == 0) {
        return;
    }

    this->callbacks[code - 1](readBuffer);
}

/// @brief Register a function to be called back on a specific code
/// @param code Can range from 0 to 15
/// @param cb Callback function
/// @return
bool FGSerialHelper::registerCallback(int code, Callback cb) {
    // Check if in valid range
    if (code < 1 || code > 16) {
        return false;
    }

    // Check if callback is already set
    if (this->callbacks[code - 1] != 0) {
        return false;
    }
    this->callbacks[code - 1] = cb;
    return true;
}

/// @brief Write a string to a specific column and row
/// @param col Column to write string to
/// @param row Row to write string to
/// @param str String to write
void FGSerialHelper::writeString(byte col, byte row, String str) {
    if (this->readMode == true) {
        return;
    }

    // Header code for writing a string is 1, followed by the desired col & row (increased by one to not include 0 byte)
    byte header[] = {1, col + 1, row + 1};
    const int headerSize = sizeof(header) / sizeof(header[0]);

    // Buffersize is size of head, content length plus one (for terminating 0 byte)
    const int frameSize = headerSize + str.length() + 1;
    byte data[frameSize];

    // Write data to buffer
    memcpy(data, header, headerSize);
    memcpy(data + headerSize, str.c_str(), str.length());

    data[frameSize - 1] = 0; // Add tail
    s.write(data, frameSize);
    s.flush();
}

/// @brief Write two lines of text to the lcd
/// @param strOne 
/// @param strTwo 
void FGSerialHelper::writeTwoString(String strOne, String strTwo) {
    if (this->readMode == true) {
        return;
    }

    // Header code for writing two strings is 2
    byte header[] = {2};
    const int headerSize = sizeof(header) / sizeof(header[0]);

    // We use 0x1D (group separator) as a text separator, because 0xA (NL line feed) gets handled inconsistently in libraries
    byte seperator[] = {0x1D};
    const int seperatorSize = sizeof(seperator) / sizeof(seperator[0]);

    const int frameSize = headerSize + strOne.length() + seperatorSize + strTwo.length() + 1;
    byte data[frameSize];

    // Fill buffer with zeroes
    memcpy(data, 0, frameSize);

    // Copy Header
    memcpy(data, header, headerSize);

    // Copy first string
    memcpy(data + headerSize, strOne.c_str(), strOne.length());

    // Copy seperator
    memcpy(data + headerSize + strOne.length(), seperator, seperatorSize);

    // Copy second string
    memcpy(data + headerSize + strOne.length() + seperatorSize, strTwo.c_str(), strTwo.length());

    data[frameSize - 1] = 0; // Add tail

    s.write(data, frameSize);
    s.flush();
}

/// @brief Data can be at max 26 characters
/// @param data
/// @param canGoUp
/// @param canGoDown
/// @param canForward
/// @param canReturn
void FGSerialHelper::writeMenu(byte *data, bool canGoUp, bool canGoDown, bool canForward, bool canReturn) {
    byte header[] = {3, 1 + (int)canGoUp, 1 + (int)canGoDown, 1 + (int)canForward, 1 + (int)canReturn};
    const int headerSize = sizeof(header) / sizeof(header[0]);

    int strSize = 0;
    for (int i = 0; data[i] != 0; i++) {
        strSize++;
    }

    const int frameSize = headerSize + strSize + 1;
    byte sendData[frameSize];
    memcpy(sendData, header, headerSize);
    memcpy(sendData + headerSize, data, strSize);

    sendData[frameSize - 1] = 0;

    s.write(sendData, frameSize);
    s.flush();
}

#endif // FGSERIALHELPER_CPP_
