#ifndef __FGSERIALHELPER_H__
#define __FGSERIALHELPER_H__

#include <Arduino.h>
#include <SoftwareSerial.h>

typedef void (*Callback)(byte *data);

class FGSerialHelper {
  private:
    static FGSerialHelper *instance;
    int pinIN;
    int pinOUT;
    bool readMode;
    SoftwareSerial s;
    Callback *callbacks;
    byte *readBuffer;

    void init();

  public:
    FGSerialHelper(int pinIN, int pinOUT, bool readMode)
        : pinIN(pinIN), pinOUT(pinOUT), readMode(readMode), s(SoftwareSerial(pinIN, pinOUT)) {
        instance = this;
        this->init();
    };
    void changeMode(bool readMode);
    bool registerCallback(int code, Callback cb);
    void handle();
    void writeString(byte col, byte row, String str);
    void writeTwoString(String strOne, String strTwo);
    void writeMenu(byte *data, bool canGoUp, bool canGoDown, bool canForward, bool canReturn);
    static FGSerialHelper &getInstance();
};

#endif // __FGSERIALHELPER_H__