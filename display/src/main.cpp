#include "FGLCDHelper.h"
#include "FGSerialHelper.h"
#include <Arduino.h>
#include <LiquidCrystal.h>
#include <SoftwareSerial.h>
#include <string.h>

FGSerialHelper sHelper(A1, A2, true);
FGLCDHelper lHelper(12, 11, 3, 4, 5, 6);

/// @brief Callback for writing text to lcd
/// @param data 
static void textHandler(byte *data) { lHelper.writeData(data);}

/// @brief Callback for writing two texts to lcd
/// @param data 
static void textTwoLineHandler(byte *data) { lHelper.writeTwoData(data);}

/// @brief Callback for writing a menu to the screen
/// @param data 
static void menuHandler(byte *data) { lHelper.writeMenu(data);}

/// @brief Callback for clearing the lcd screen
/// @param data 
static void flusher(byte *data) {
    free(data); // Buffer can directly be freed, as we dont need it beyond this point
    lHelper.lcd.clear();
    lHelper.lcd.flush();
}

void setup() {
    Serial.begin(9600);
    // Text IO
    sHelper.registerCallback(1, textHandler);
    sHelper.registerCallback(2, textTwoLineHandler);
    
    // Menu
    sHelper.registerCallback(3, menuHandler);
    
    // Utilities
    sHelper.registerCallback(7, flusher);
    
    lHelper.lcd.begin(16,2);
    lHelper.lcd.clear();

    // Set characterset
}

void loop() {
    // Continuously handle serial input
    sHelper.handle();
    delay(50);
}
