
# Tools
## PlatformIO
Name: PlatformIO IDE  
Id: platformio.platformio-ide  
Version: 3.3.1  
Publisher: PlatformIO  
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=platformio.platformio-ide  
### Dependecies
This will need to have python >= 3.6 installed

## Formatter
Name: Clang-Format  
Id: xaver.clang-format  
Version: 1.9.0  
Publisher: Xaver Hellauer  
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=xaver.clang-format  

### Dependecies
This will need to have ``clang-format`` installed
```bash
sudo apt install clang-format
```