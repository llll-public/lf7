#ifndef __FGRFIDHELPER_H__
#define __FGRFIDHELPER_H__

#define SS_PIN 10
#define RST_PIN 9

#include <MFRC522.h>
#include <SPI.h>

class FGRFIDHelper {
  private:
    static FGRFIDHelper *instance;
    MFRC522 mfrc522;
    MFRC522::MIFARE_Key key;
    bool initialized = false;
    byte *buffer;
    byte *splitNumber;
    void stopRFID();
    byte bufferLength = 18;

  public:
    FGRFIDHelper();
    void init();
    bool ready();
    int fromSplitBytes(byte data[]);
    byte* toSplitBytes(int number);
    
    byte* read(int block, bool print = false, bool printData = false);
    bool write(int block, byte data[], bool print = false);
    bool writeNumber(int block, int number);
    int readNumber(int block);
    bool waitForWriteNumber(int block, int number);
    int waitForReadNumber(int block);
    static FGRFIDHelper &getInstance();
};

#endif // __FGRFIDHELPER_H__