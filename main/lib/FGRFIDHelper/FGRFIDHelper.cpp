#ifndef FGRFIDHELPER_CPP_
#define FGRFIDHELPER_CPP_

#include "FGRFIDHelper.h"

FGRFIDHelper *FGRFIDHelper::instance = nullptr;

/// @brief Get singleton instance
/// @return Instance
FGRFIDHelper &FGRFIDHelper::getInstance() {return *instance;};

FGRFIDHelper::FGRFIDHelper() {
    mfrc522 = MFRC522(SS_PIN, RST_PIN);
    instance = this;
}

/// @brief Initialiyer function
void FGRFIDHelper::init() {
    SPI.begin();
    mfrc522.PCD_Init();

    // Set rfid key to zero
    for (byte i = 0; i < 6; i++) {
        key.keyByte[i] = 0xFF;
    }

    initialized = true;
}

/// @brief Whether or not the rfid is ready to handle data
/// @return true | false
bool FGRFIDHelper::ready() {
    if (!initialized) {
        Serial.println("Helper has not yet been initialized");
        return false;
    }
    if (!mfrc522.PICC_IsNewCardPresent() || !mfrc522.PICC_ReadCardSerial()) {
        return false;
    }
    return true;
}

/// @brief Read data from a specific block on the rfid chip
/// @param block Which block to read from
/// @param print Whether or not to print error messages to serial
/// @param printData Whether or not to print data to serial
/// @return Buffer with the read data
byte *FGRFIDHelper::read(int block, bool print = false, bool printData = false) {
    // Clear buffer
    buffer = new byte[bufferLength];
    memset(buffer, 0, bufferLength);

    // Authenticate with rfid chip
    MFRC522::StatusCode status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, block, &key, &(mfrc522.uid));
    if (status != MFRC522::STATUS_OK) {
        if (print) {
            Serial.print(F("Authentication failed: "));
            Serial.println(mfrc522.GetStatusCodeName(status));
        }
        this->stopRFID();
        return {};
    }

    // Read data to buffer, handling status
    status = mfrc522.MIFARE_Read(block, buffer, &bufferLength);
    if (status != MFRC522::STATUS_OK) {
        if (print) {
            Serial.print(F("Reading failed: "));
            Serial.println(mfrc522.GetStatusCodeName(status));
        }
        this->stopRFID();
        return {};
    }
    this->stopRFID();

    if (printData) {
        Serial.print(F("Data in block "));
        Serial.print(block);
        Serial.println(F(":"));
        for (byte i = 0; i < 16; i++) {
            Serial.print(buffer[i] < 0x10 ? " 0" : " ");
            Serial.print(buffer[i], HEX);
        }
        Serial.println();
    }
    return buffer;
}

/// @brief Write data to a specific block on the rfid chip
/// @param block Which block to write data to
/// @param data Data to write
/// @param print Whether or not to print error messages to serial
/// @return Success status
bool FGRFIDHelper::write(int block, byte data[], bool print = false) {
    // Authenticate with rfid chip
    MFRC522::StatusCode status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, block, &key, &(mfrc522.uid));
    if (status != MFRC522::STATUS_OK) {
        if (print) {
            Serial.print(F("Authentication failed: "));
            Serial.println(mfrc522.GetStatusCodeName(status));
        }
        this->stopRFID();
        return false;
    }

    // Write data to the block and handle status
    status = mfrc522.MIFARE_Write(block, data, 16);
    if (status != MFRC522::STATUS_OK) {
        if (print) {
            Serial.print(F("Writing failed: "));
            Serial.println(mfrc522.GetStatusCodeName(status));
        }
        this->stopRFID();
        return false;
    }

    if (print) {
        Serial.println(F("Data written to block successfully!"));
    }
    this->stopRFID();
    return true;
}

/// @brief Post steps for rfid data transmissions
void FGRFIDHelper::stopRFID() {
    mfrc522.PICC_HaltA();
    mfrc522.PCD_StopCrypto1();
}

/// @brief Parse a 2 byte array holding a split 16 bit int to a valid 16 bit int
/// @param data
/// @return 16 bit int
int FGRFIDHelper::fromSplitBytes(byte data[]) {
    int d0 = data[0];
    int d1 = data[1];
    return (d0 << 8) | d1;
}

/// @brief Split a at max 16 bit int to its byte components
/// @param number
/// @return Split 16 bit int
byte *FGRFIDHelper::toSplitBytes(int number) {
    // Only handle up to 16 bit numbers
    if (number > 65535) {
        return {};
    }
    byte superior = (number >> 8) & 0xFF;
    byte inferior = number & 0xFF;

    // This function returns an array thus we'll need to return a reference to an existing memory location in this function
    // Clear buffer
    splitNumber = new byte[bufferLength];
    memset(splitNumber, 0, bufferLength);

    splitNumber[0] = superior;
    splitNumber[1] = inferior;
    return splitNumber;
}

/// @brief Write a 16 bit number to a specific block
/// @param block Block to write to
/// @param number 16 bit number to write
/// @return Success status
bool FGRFIDHelper::writeNumber(int block, int number) {
    byte *data = this->toSplitBytes(number); // Split int to bytes
    return this->write(block, data);
}

/// @brief Read a 16 bit number from a specific block
/// @param block Block to read from
/// @return 16 bit number
int FGRFIDHelper::readNumber(int block) { return this->fromSplitBytes(this->read(block)); }

/// @brief Blocking number read, which waits for data transmission
/// @param block Block to read from
/// @return 16 bit number
int FGRFIDHelper::waitForReadNumber(int block) {
    while (!this->ready()) {
        delay(50);
    }
    return this->readNumber(block);
}

/// @brief Blocking number write, which waits for data transmission
/// @param block Block to write to
/// @param number 16 bit number to write
/// @return Success status
bool FGRFIDHelper::waitForWriteNumber(int block, int number) {
    while (!this->ready()) {
        delay(50);
    }
    return this->writeNumber(block, number);
}

#endif // FGRFIDHELPER_CPP_