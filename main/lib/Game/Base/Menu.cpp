#ifndef MENU_CPP_
#define MENU_CPP_

#include "Menu.h"

Menu *Menu::instance = nullptr;

/// @brief Get singleton instance
/// @return Instance
Menu &Menu::getInstance() {return *instance;}

/// @brief Calls serialhelper function with correct parameters deducted from the provided item
/// @param item 
void Menu::writeMenuItem(MenuItem *item) {
    bool canNext = item->next != nullptr;
    bool canPrev = item->prev != nullptr;
    bool canReturn = item->parent != nullptr;
    bool canForward = item->child != nullptr;
    sHelper.writeMenu(item->text, canPrev, canNext, canForward, canReturn);
}

#endif // MENU_CPP_