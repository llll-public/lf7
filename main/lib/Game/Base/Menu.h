#ifndef __MENU_H__
#define __MENU_H__

#include "FGMembraneHelper.h"
#include "FGSerialHelper.h"

struct MenuItem {
    MenuItem *next = nullptr;
    MenuItem *prev = nullptr;
    MenuItem *child = nullptr;
    MenuItem *parent = nullptr;
    byte* text;
    int code;
};

class Menu {
  private:
    static Menu *instance;
    MenuItem *items;
    FGSerialHelper &sHelper;
    FGMembraneHelper &mHelper;

  public:
    Menu() : sHelper(FGSerialHelper::getInstance()), mHelper(FGMembraneHelper::getInstance()) {instance = this;};
    void writeMenuItem(MenuItem *item);
    static Menu &getInstance();
};

#endif // __MENU_H__