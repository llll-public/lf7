#ifndef __GAME_H__
#define __GAME_H__

#include "FGRFIDHelper.h"
#include "FGSerialHelper.h"
#include "Menu.h"

class Game {
  private:
    Menu *menu;
    FGSerialHelper *sHelper;
    FGRFIDHelper *rHelper;
    FGMembraneHelper *mHelper;
    MenuItem *startItem = nullptr;
    int getMenuSelection(MenuItem *item);

  public:
    void init();
    void run();
    void startGame(int gameNumber, int balance);
    Game() {
        menu = &Menu::getInstance();
        sHelper = &FGSerialHelper::getInstance();
        rHelper = &FGRFIDHelper::getInstance();
        mHelper = &FGMembraneHelper::getInstance();
        init();
    }
};

#endif // __GAME_H__