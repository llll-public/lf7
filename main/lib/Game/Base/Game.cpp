#ifndef GAME_CPP_
#define GAME_CPP_

#include "Game.h"
#include "Menu.h"

/// @brief Initializes main menu of the game
void Game::init() {
    // Create necessary menuitems which are needed for the main menu

    startItem = new MenuItem();
    startItem->text = (byte *)"Press Enter";

    // Start Game
    MenuItem *startGameItem = new MenuItem();
    startGameItem->text = (byte *)"Start Game";
    startGameItem->code = 1;
    startGameItem->parent = startItem;

    // Check Money
    MenuItem *checkMoneyItem = new MenuItem();
    checkMoneyItem->text = (byte *)"Check Balance";
    checkMoneyItem->prev = startGameItem;
    checkMoneyItem->parent = startItem;
    checkMoneyItem->code = 2;

    startGameItem->next = checkMoneyItem;
    startItem->child = startGameItem;
}

/// @brief Game logic handler
void Game::run() {
    /**
     * Selection codes
     * -1 	= Error
     * 1 	= Start Game
     * 2 	= Check Money
     */
    int selection = getMenuSelection(startItem);
    switch (selection) {
    // Handles the start game menu item
    case (1): {
        sHelper->writeTwoString("Please hold the", "chip to reader");
        int balance = rHelper->waitForReadNumber(2);
        delay(500);
        if (balance <= 0) {
            sHelper->writeTwoString("Not enough", "balance!");
            mHelper->getKey();
            return;
        }

        MenuItem *gameItem = new MenuItem();
        gameItem->text = (byte *)"Select Game";

        MenuItem *pairGameItem = new MenuItem();
        pairGameItem->text = (byte *)"Pair Game";
        pairGameItem->code = 1;
        pairGameItem->parent = gameItem;
        gameItem->child = pairGameItem;

        MenuItem *slotsItem = new MenuItem();
        slotsItem->text = (byte *)"Slots Game";
        slotsItem->prev = pairGameItem;
        slotsItem->parent = gameItem;
        slotsItem->code = 2;

        pairGameItem->next = slotsItem;

        MenuItem *numberItem = new MenuItem();
        numberItem->text = (byte *)"Number Game";
        numberItem->prev = slotsItem;
        numberItem->parent = gameItem;
        numberItem->code = 3;

        slotsItem->next = numberItem;

        int selection = getMenuSelection(gameItem);
        startGame(selection, balance);
        break;
    }
    // Handles the balance menu item and will allow the user to read the balance from the rfid chip
    case (2): {
        // Get the user to hold the chip to the reader
        sHelper->writeTwoString("Please hold the", "chip to reader");
        delay(500);
        int balance = rHelper->waitForReadNumber(2);

        // Display thr current balance to the lcd
        sHelper->writeTwoString("Current balance:", String(balance));
        mHelper->getKey(); // Wait for input;
        delay(500);
        break;
    }
    }
}

/// @brief Handles display and selection of a menu structure as defined by the item
/// @param item
/// @return
int Game::getMenuSelection(MenuItem *item) {
    // Write menu to screen
    menu->writeMenuItem(item);
    delay(1000); // Wait for data to be send before allowing user input

    int toReturn = -1; // Define failsafe as -1, which will indicate an error occurred

    // Continuously handle key inputs
    while (true) {
        int selection = mHelper->getKey(); // Get key

        // Should select child
        if (selection == 35 || selection == 54) {
            if (item->child == nullptr) {
                // If no child is available, this item should be selected. We return the items defined code
                toReturn = item->code;
                break;
            } else {
                // Recursive call to get final selection with child item
                toReturn = getMenuSelection(item->child);
                break;
            }
        }

        // Should go back
        if (selection == 42 || selection == 52) {
            if (item->parent == nullptr) {
                continue; // Do nothing
            } else {
                // Recursive call to get final selection with parent item
                toReturn = getMenuSelection(item->parent);
                break;
            }
        }

        // Should go up
        if (selection == 50) {
            if (item->prev == nullptr) {
                continue; // Do nothing
            } else {
                // Recursive call to get final selection with previous item
                toReturn = getMenuSelection(item->prev);
                break;
            }
        }

        // Should go down
        if (selection == 56) {
            if (item->next == nullptr) {
                continue; //  Do nothing
            } else {
                // Recursive call to get final selection with next item
                toReturn = getMenuSelection(item->next);
                break;
            }
        }
    }
    return toReturn;
}

/// @brief Sort an array of integers in ascending order using bubble sort
/// @note Arduino cpp does not provide build in sorting functions, thus a custom function was needed
/// @see https://en.wikipedia.org/wiki/Bubble_sort
/// @param arr
/// @param size
void bubbleSort(int arr[], int size) {
    for (int i = 0; i < size - 1; i++) {
        for (int j = 0; j < size - i - 1; j++) {
            if (arr[j] > arr[j + 1]) {
                int temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
}

/// @brief Starts a game identified by its number
/// @param gameNumber
/// @param balance
void Game::startGame(int gameNumber, int balance) {
    delay(500);

    // Get bid
    sHelper->writeString(0, 0, "Input your bid!");
    int bid = atoi(mHelper->getInput());
    sHelper->writeTwoString("Your bid:", String(bid));
    char key = mHelper->getKey();
    if (key == '*') {
        return;
    }

    // Check if user can afford the bid, else stop
    if (balance - bid < 0) {
        sHelper->writeTwoString("Not enough", "balance!");
        mHelper->getKey();
        return;
    }

    switch (gameNumber) {
    // Pairs
    case (1): {
        delay(500);

        // Get guess from user
        sHelper->writeTwoString("Input guess for", "pairs count");
        int guessCount = atoi(mHelper->getInput());

        randomSeed(analogRead(0)); // Update random seed

        // Generate 10 random numbers
        int numbers[10];
        for (int i = 0; i < 10; i++) {
            numbers[i] = random(0, 10);
        }

        // Sort the numbers for easier processing and nicer display
        bubbleSort(numbers, 10);

        // Create a string from number array to be displayed on the lcd
        String numberString = "";
        for (int i = 0; i < 10; i++) {
            numberString += String(numbers[i]);
        }

        // Count the number of pairs in the number array. 00011 will count as three pairs: 00, 00, 11
        int count = 0;
        for (int i = 0; i < 10; i++) {
            // Compare current number to each of the following numbers
            for (int j = i + 1; j < 10; j++) {
                if (numbers[i] == numbers[j]) {
                    count++; // Numbers are equal --> pair found
                    // Once a pair is found, increment j until the pair is ended.
                    while (j < 10 && numbers[i] == numbers[j]) {
                        j++;
                    }
                }
            }
        }

        // Show user the numbers
        sHelper->writeTwoString("Numbers:", numberString);
        mHelper->getKey();

        // Show user a summary of their guess and actual count
        sHelper->writeTwoString("Guess: " + String(guessCount), "Actual: " + String(count));
        mHelper->getKey();

        // Handle win & loose money
        if (guessCount == count) {
            sHelper->writeTwoString("You win!", "Hold RFID Chip");
            rHelper->waitForWriteNumber(2, balance + bid);
        } else {
            sHelper->writeTwoString("You loose!", "Hold RFID Chip");
            rHelper->waitForWriteNumber(2, balance - bid);
        }

        break;
    }
    // Slots
    case (2): {
        // Show rules to the user
        sHelper->writeTwoString("Points:", "0:1 5:10 7:50");
        mHelper->getKey();
        sHelper->writeTwoString("Score >= 60", "points to win");
        mHelper->getKey();

        randomSeed(analogRead(0)); // Update random seed

        int numbers[10];
        int score = 0;
        // Generate 10 different numbers and update score accordingly
        for (int i = 0; i < 10; i++) {
            numbers[i] = random(0, 10);
            switch (numbers[i]) {
            case (0): {
                score += 1;
                break;
            }
            case (5): {
                score += 10;
                break;
            }
            case (7): {
                score += 50;
                break;
            }
            }
        }

        // Create a string from number array to be displayed on the lcd
        String numberString = "";
        for (int i = 0; i < 10; i++) {
            numberString += String(numbers[i]);
        }

        // Show user the numbers
        sHelper->writeTwoString("Numbers:", numberString);
        mHelper->getKey();
        // Show user a summary of the score and the required score
        sHelper->writeTwoString("Score: " + String(score), "Required: 60");
        mHelper->getKey();

        // Handle win & loose money
        if (score >= 60) {
            sHelper->writeTwoString("You win!", "Hold RFID Chip");
            rHelper->waitForWriteNumber(2, balance + bid);
        } else {
            sHelper->writeTwoString("You loose!", "Hold RFID Chip");
            rHelper->waitForWriteNumber(2, balance - bid);
        }
        break;
    }
    // Number
    case (3): {
        delay(500);

        // Get guess input
        sHelper->writeTwoString("Input guess", "0-9");
        int number = atoi(mHelper->getInput());

        randomSeed(analogRead(0)); // Update random seed

        int randomNumber = random(0, 10);

        // Show user a summary of their guess and the actual number
        sHelper->writeTwoString("Guess: " + String(number), "Actual: " + String(randomNumber));
        mHelper->getKey();

        // Handle win & loose money
        if (number == randomNumber) {
            sHelper->writeTwoString("You win!", "Hold RFID Chip");
            rHelper->waitForWriteNumber(2, balance + bid);
        } else {
            sHelper->writeTwoString("You loose!", "Hold RFID Chip");
            rHelper->waitForWriteNumber(2, balance - bid);
        }
        break;
    }
    }
}

#endif // GAME_CPP_