#ifndef __FGMEMBRANEHELPER__
#define __FGMEMBRANEHELPER__

#include <Arduino.h>
#include <Keypad.h>

class FGMembraneHelper {
  private:
    static FGMembraneHelper *instance;
    static const byte rows = 4;
    static const byte cols = 3;
    byte rowPins[rows] = {8, 7, 6, 5};
    byte colPins[cols] = {4, 3, 2};
    Keypad keypad;

    char keys[rows][cols] = {{'1', '2', '3'}, {'4', '5', '6'}, {'7', '8', '9'}, {'*', '0', '#'}};
    char *inputBuffer;

  public:
    FGMembraneHelper() : keypad(makeKeymap(keys), rowPins, colPins, rows, cols) {
      instance = this;
        keypad.setDebounceTime(50);
        inputBuffer = new char[5];
    };
    char getKey();
    char *getInput();
    static FGMembraneHelper &getInstance();
};

#endif // __FGMEMBRANEHELPER__