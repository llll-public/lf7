#ifndef FGMEMBRANEHELPER_CPP_
#define FGMEMBRANEHELPER_CPP_

#include "FGMembraneHelper.h"

FGMembraneHelper *FGMembraneHelper::instance = nullptr;

/// @brief Get singleton instance
/// @return Instance
FGMembraneHelper &FGMembraneHelper::getInstance() { return *instance; }

/// @brief Get key press
/// @details The Keyboard libraries implementation of waitForKey seems to be broken (caching previously pressed key, without
/// freeing it). Thus this function needed to be implemented
/// @return Key which was pressed
char FGMembraneHelper::getKey() {
    int key = NO_KEY;

    // Only return key, once a valid key was actually pressed
    do {
        key = keypad.getKey();
        delay(50);
    } while (key == NO_KEY);
    return key;
}

/// @brief Blocking read of input. Submit with octothorp, Delete with asterisk
/// @return The string input limited to 5 characters
char *FGMembraneHelper::getInput() {
    memset(inputBuffer, 0, 5);
    // Reinitalize keymap each time because softwareserial breaks configuartion
    keypad = Keypad(makeKeymap(keys), rowPins, colPins, rows, cols);

    int index = 0;
    while (true) {
        char c = this->getKey();
        // Read until octothorp is pressed
        if (c == '#') {
            break;
        } else if (c == '*') {
            // Clear current entry from buffer and step back
            inputBuffer[index] = 0;
            index = index > 0 ? index - 1 : index;
        } else {
            // Write character to buffer and stepforward
            inputBuffer[index] = c;
            index = index < 4 ? index + 1 : index;
        }
    }

    return inputBuffer;
}

#endif // FGMEMBRANEHELPER_CPP_
