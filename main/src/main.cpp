#include "Base/Menu.h"
#include "FGMembraneHelper.h"
#include "FGRFIDHelper.h"
#include "FGSerialHelper.h"
#include <Arduino.h>
#include <SoftwareSerial.h>
#include "Base/Game.h"

FGRFIDHelper rfidHelper;
FGMembraneHelper memHelper;
FGSerialHelper sHelper(A1, A2, false);
Menu *m = new Menu();
MenuItem *mI = new MenuItem();

void setup() {
    Serial.begin(9600);

    rfidHelper = FGRFIDHelper::getInstance();
    rfidHelper.init();
    memHelper = FGMembraneHelper::getInstance();
    delay(1500); // Wait for everything to initalize
}

void loop() {
    // Continuously restart game if it exits
    Game g = Game();
    g.run();
}